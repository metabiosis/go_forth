#!/bin/bash
# (C) 2006 Aymeric Mansoux
# aym3ric -at- goto10 -dot- org
# GNU GPL License

# script called by wmaker.sh when asked in /dyne/dyne.cfg
# It is needed to provide an alternate desktop organization and start various
# things the p:d way.
# As a result it also modify the startx command.

# dependencies

source /lib/dyne/utils.sh

# shortcuts

DYNE_CFG_FILE=$DYNE_SYS_MNT/dyne.cfg

# ----------------------------------------------------------------------------
# functions

# Check windows manager preferences
# We first check for user preferences in ~/.xinitrc, if not found we check for
# system wide preferences in dyne.cfg, if not found we start with fluxbox + rox

which_wm() {
	if ! [ $WINDOWMANAGER ]; then 				
		WINDOWMANAGER=`get_config window_manager`	
		if ! [ $WINDOWMANAGER ]; then
			WINDOWMANAGER=fluxrox
    		fi
	fi
	}

# p:d windows manager startup

startXpdwm() {

	if [ $WINDOWMANAGER = fluxrox ]; then 
		startx_fluxrox
	elif [ $WINDOWMANAGER = fluxbox ]; then 
		startx_fluxbox 
	elif [ $WINDOWMANAGER = evil ]; then 
		startx_evil
	elif [ $WINDOWMANAGER = xfce4 ]; then
                startx_xfce4
	elif [ $WINDOWMANAGER = wmaker ]; then
                startx_wmaker
	fi
	}

# fluxbox + rox

startx_fluxrox() {

	# start qjackctl
	qjackctl &
	
	# prepare ROX filer for its first start
	mkdir -p $HOME/.config/rox.sourceforge.net/ROX-Filer
	#cp $ROXPD $HOME/.config/rox.sourceforge.net/ROX-Filer/
	
    	# the multiple desktop pager
    	(sleep 5; fbpager &)&

    	# our beloved "pure" splashscreen ;)
    	if ! [ -r $HOME/.nosplash ]; then
       		puredynesplash &
    	fi

    	# start our ROX filer with pinboard and panel
    	(sleep 3; rox -p Default &)&

	# turn off the screensaver
  	(sleep 1; xset s off -dpms &)&

  	source /etc/LANGUAGE

  	if [ $KEYB ]; then
    		(sleep 2; /usr/X11R6/bin/setxkbmap $KEYB &)&
  	fi

	# overide wmaker.sh hardcoded settings
	cp /opt/pureiso/skel/.fluxbox/init ~/.fluxbox/
	cp /opt/pureiso/skel/.fluxbox/fbpager ~/.fluxbox/
	cp /opt/pureiso/skel/.fluxbox/keys ~/.fluxbox/	
	cat ~/.config/rox.sourceforge.net/ROX-Filer/pan_Default | \
		sed s/dyne:II/pure:dyne/g | \
		sed 's/\/bin\/dynesplash/\/opt\/pureiso\/bin\/puredynesplash/g' > \
		/tmp/pan
	mv /tmp/pan ~/.config/rox.sourceforge.net/ROX-Filer/pan_Default
	
  	# rename the fluxbox menu
	DYNE_SYS_VER="`cat /usr/etc/DYNEBOLIC`"
	PDBUILD=`awk '/BUILD/ {print $3}' /opt/puredevel/milk_factory/boot.msg`
		
	cat /etc/fluxbox/menu | \
		sed "s/(dyne:II)/(pure:dyne $DYNE_SYS_VER.$PDBUILD)/g" > /tmp/menu
	mv /tmp/menu /etc/fluxbox/menu

	# start the window manager
	bsetroot -solid black
 	
	# welcome glitch
        welcome_glitch.sh

	exec fluxbox

	}

# fluxbox alone

startx_fluxbox() { 

        # start qjackctl
        qjackctl &
	
	# the multiple desktop pager
        (sleep 5; fbpager &)&

        # our beloved "pure" splashscreen ;)
        if ! [ -r $HOME/.nosplash ]; then
                puredynesplash &
        fi

        # turn off the screensaver
        (sleep 1; xset s off -dpms &)&

        source /etc/LANGUAGE

        if [ $KEYB ]; then
                (sleep 2; /usr/X11R6/bin/setxkbmap $KEYB &)&
        fi

        # overide wmaker.sh hardcoded settings
        cp /opt/pureiso/skel/.fluxbox/init ~/.fluxbox/
        cp /opt/pureiso/skel/.fluxbox/fbpager ~/.fluxbox/
        cp /opt/pureiso/skel/.fluxbox/keys ~/.fluxbox/

        # rename the fluxbox menu
        DYNE_SYS_VER="`cat /usr/etc/DYNEBOLIC`"
	PDBUILD=`awk '/BUILD/ {print $3}' /opt/puredevel/milk_factory/boot.msg`

        cat /etc/fluxbox/menu | \
                sed "s/(dyne:II)/(pure:dyne $DYNE_SYS_VER.$PDBUILD)/g" > /tmp/menu
        mv /tmp/menu /etc/fluxbox/menu

        # start the window manager
        fbsetbg -c /opt/pureiso/share/background/rox_background_01.png
        
	# welcome glitch
        welcome_glitch.sh	

	exec fluxbox

	}

# evilwm

startx_evil() {

	cd /root/metabiosis/07-multiply/

	# Set the background and root cursor shape
	xsetroot -solid \#101020
	xsetroot -cursor_name top_left_arrow

	# Start evilwm 
	evilwm -snap 10 -fg \#404080 -bg \#202040 -bw 0 \
	 -app SDL_App -s -g "+0+0" \
	 -app xterm -s -g "+481+0" &

	xterm -cr \#111 -fg \#88A -bg \#112 \
	 -fn -misc-fixed-medium-r-normal--8-60-100-100-c-50-iso8859-1 \
	 -geometry 31x60 -e "pf multiply.pf"
	
	#xterm
	# -app wmnet -s -g "+0+64"

	# welcome glitch
	#welcome_glitch.sh &
	
	# Start gadgets
	#bubblemon &
	# -W bla0 -> makes wmnet listen to bla0 instead of default eth0 
	#wmnet -l -r \#6060FF
	}

# xfce4

startx_xfce4() {
	
	echo "TODO!"

	}

# wmaker

startx_wmaker() {

        echo "TODO!"

        }


# ----------------------------------------------------------------------------
# sequence

which_wm
startXpdwm

# README - metabiosis interface 
# this readme explains how the interface of the "go forth & *" installation is
# working

# HOW TO START THE APPLICATION
To start the metabiosis interface and make it ready to insert new packets into
the ecosystems, follow the following steps:

1. when all the ecosystems are up and running, and you've modified all the
IP's to match the local system, launch pf on the interface machine:

user@computer~ $ pf

2. load the pf script that inserts new packets into ecosystem 1, and connect
it to ecosystem 1 :

> "new-packet.pf" load
> connect

3. launch an mrxvt terminal from a terminal (not from the menu!!!) and launch 
the interface (fullscreen): 

user@computer~ $ ./interface.sh

Now the system is ready to take user input and insert new packets into the 3
ecosystems. 

# MRXVT
For the installation "go forth & *" we use an mrxvt terminal to display the
interface. The .mrxvtrc file is in the 07-multiply/interface folder. To
increase the font-size in the terminal, do: 

CTRL +

# PREPARATION OF THE MACHINE
1. dock pure:dyne
2. nest pure:dyne
3. get all files from: http://devel.goto10.org/ (metabiosis/07-multiply)
4. copy the .mrxvtrc file to your home folder
5. copy the .dialogrc file into your home folder too




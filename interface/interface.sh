#!/bin/bash

trap " " SIGHUP SIGINT SIGQUIT SIGABRT SIGALRM SIGTERM

function mainmenu {
echo "metabiosis"
	dialog --title "Metabiosis START " --ok-label "  START  " --msgbox "\n               
                 -----------------------------\n
                 g   o       f   o   r   t   h\n
                 -----------------------------\n
                   .d8888b.\n
                  d88P  \"88b            o\n
                  Y88b. d88P           d8b\n
                   \"Y8888P\"           d888b\n
                  .d88P88K.d88P   \"Y888888888P\"\n
                  888\"  Y888P\"      \"Y88888P\"\n
                  Y88b .d8888b      d88P\"Y88b\n
                  \"Y8888P\" Y88b    dP\"     \"Yb\n
                \n
                 -----------------------------\n
                 metabiosis  .  goto10  .  org\n
                 -----------------------------\n
" 20 70 
sel=$?
 case $sel in
    0) name ;;
 esac
}
											
# NAME OF CREATOR
function name
	{
	dialog --cancel-label "back to START" --inputbox "Please enter your name, and press OK to confirm" 20 70 \
	 2> for_pf
	sel=$?
	name=`cat for_pf`
	rm -f for_pf 
	if test $sel -eq 1 ; then
		mainmenu
	elif test $sel -eq 0 ; then
	 	if [ -n "$name" ] ; then
		packet_name
	  	else dialog --infobox "\nPlease enter your name before pressing OK\n \n
\n\n\n\n\n\n
                                          .-----.\n
                                         / oh oh \ \n
                                         \  ...  / \n
                                         /'-----' \n
                                     __ '  __ !!! \n
                                    ('')  (++) \n
                               '  '\" '''\"\"'''\"\"\"\"\"'' ''\"\" \" \n
		
		" 20 70
		sleep 3
		name
		fi 
	fi
	}
# NAME OF PACKET
function packet_name
	{
	dialog --cancel-label "back to START" --inputbox "Please give your packet a name" 20 70 \
	 2> for_pf
	sel=$?
	name_packet=`cat for_pf`
	rm -f for_pf
	if test $sel -eq 1 ; then
		mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$name_packet" ] ; then
		speed
		else dialog --infobox "\nPlease enter a name for your packet, before pressing OK\n\n
\n\n\n\n\n\n
                                          .-----.\n
                                         / oh oh \ \n
                                         \  ...  / \n
                                         /'-----' \n
                                     __ '  __ !!! \n
                                    ('')  (++) \n
                               '  '\" '''\"\"'''\"\"\"\"\"'' ''\"\" \" \n		

		" 20 70
		sleep 3
		packet_name
		fi
	fi
	}
# DNA : SPEED 
function speed	
	{
	dialog --cancel-label "back to START" --radiolist "Is $name_packet a carefull explorer or a stressed out speed freak? Please indicate $name_packet's speed by checking one of the numbers below." \
	 20 70 11 "0" "can hardly be called alive" off "1" "seems to be waiting for ..." off "2" "only for the very patient" off "3" "can't keep up with the world" off "4" "does things at its own pace" off "5" "leaves early, and is always on time" off "6" "keeps a good tempo" off "7" "works out to stay in shape" off "8" "drinks kerosine for breakfast" off "9" "is used in races (and wins)" off "10" "what was that?" off \
	 2> for_pf 
	sel=$?
	speed=`cat for_pf`
	rm -f for_pf
	if test $sel -eq 1 ; then
		mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$speed" ] ; then
		travel
		else dialog --infobox "\nYour choice was not registered, please click on the option of your choice\n\n
\n\n\n\n\n\n
                                          .-----.\n
                                         / oh oh \ \n
                                         \  ...  / \n
                                         /'-----' \n
                                     __ '  __ !!! \n
                                    ('')  (++) \n
                               '  '\" '''\"\"'''\"\"\"\"\"'' ''\"\" \" \n		

		" 20 70
		sleep 3
		speed			
		fi
	fi
	}
# DNA : TRAVEL
function travel
	{
	dialog --cancel-label "back to START" --radiolist "Always on the move or no place like home? Does $name_packet like to travel?" \
	 20 70 11 "0" "not interested in EVER leaving home" off "1" "only leaves to get food" off "2" "goes on short walks" off "3" "has never left town" off "4" "visits family if it must" off "5" "takes the occasional trip" off "6" "goes on holiday once a year" off "7" "is known for its train fetish" off "8" "a regular rolling stone" off "9" "cannot stay in the same place long" off "10" "space... the final frontier..." off \
	 2> for_pf 
	sel=$?
	travel=`cat for_pf`
	rm -f for_pf
	if test $sel -eq 1 ; then
		mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$travel" ] ; then
		social
		else dialog --infobox "\nYour choice was not registered, please click on the option of your choice\n\n
\n\n\n\n\n\n
                                          .-----.\n
                                         / oh oh \ \n
                                         \  ...  / \n
                                         /'-----' \n
                                     __ '  __ !!! \n
                                    ('')  (++) \n
                               '  '\" '''\"\"'''\"\"\"\"\"'' ''\"\" \" \n
		
		" 20 70
		sleep 3
		travel			
		fi
	fi
	}
# DNA : SOCIAL
function social
	{
	dialog --cancel-label "back to START" --radiolist "Sociopath or every packets friend? Please indicate $name_packet's social skills:" \
	 20 70 11 "0" "absolutely HATES other packets" off "1" "one is company, two is a crowd" off "2" "is extremely shy" off "3" "prefers to be left alone" off "4" "cannot be bothered to interact" off "5" "has a few (real) friends" off "6" "feels better with others around" off "7" "wishes the bar would never close" off "8" "cannot stand to be alone" off "9" "loves everything that moves" off "10" "Always stalking others" off \
	 2> for_pf
	sel=$?
	social=`cat for_pf`
	rm -f for_pf
	if test $sel -eq 1 ; then
		mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$social" ] ; then
		reproduction
		else dialog --infobox "\nYour choice was not registered, please click on the option of your choice\n\n
\n\n\n\n\n\n
                                          .-----.\n
                                         / oh oh \ \n
                                         \  ...  / \n
                                         /'-----' \n
                                     __ '  __ !!! \n
                                    ('')  (++) \n
                               '  '\" '''\"\"'''\"\"\"\"\"'' ''\"\" \" \n		

		" 20 70
		sleep 3
		social			
		fi
	fi
	}
# DNA : REPRODUCTION
function reproduction
	{
	dialog --cancel-label "back to START" --radiolist "One of the most fundamental questions in the life of a packet... To reproduce or not to reproduce? $name_packet ..."\
	 20 70 2 "0" "has no parental instincts at all" off "1" "thinks children are the future" off\
	  2> for_pf
	sel=$?
	reproduction=`cat for_pf`
	rm -f for_pf
	if test $sel -eq 1 ; then
		mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$reproduction" ] ; then
		goodbye
		else dialog --infobox "\nYour choice was not registered, please click on the option of your choice\n\n
\n\n\n\n\n\n
                                          .-----.\n
                                         / oh oh \ \n
                                         \  ...  / \n
                                         /'-----' \n
                                     __ '  __ !!! \n
                                    ('')  (++) \n
                               '  '\" '''\"\"'''\"\"\"\"\"'' ''\"\" \" \n		

		" 20 70
		sleep 3
		reproduction			
		fi
	fi
	}
# THANK YOU AND GOODBYE SCREEN
function goodbye
	{
	dialog --infobox "\n
                 -----------------------------\n
                 t   h  a  n  k        y  o  u  \n
                 -----------------------------\n
                   .d8888b.\n
                  d88P  \"88b            o\n
                  Y88b. d88P           d8b\n
                   \"Y8888P\"           d888b\n
                  .d88P88K.d88P   \"Y888888888P\"\n
                  888\"  Y888P\"      \"Y88888P\"\n
                  Y88b .d8888b      d88P\"Y88b\n
                  \"Y8888P\" Y88b    dP\"     \"Yb\n
                \n
                 -----------------------------\n
                 &         g  o  o  d  b  y  e  \n
                 -----------------------------\n
	\n
Your packet will be arriving in ecosystem 1 shortly\n"\
	 20 70
	sleep 4
	sed -i '4d' temp-store.pf
	sed -i "/~GENES/a\ \(\"$name\" \"$name_packet\" $speed $travel $social $reproduction\) GENES !" temp-store.pf
	sed -i '2d' temp-store.pf
	sed -i "/~GATE/a\1 inserted? \!" temp-store.pf
	
	cp temp-store.pf temp-store-real.pf
 
	sleep 4
	mainmenu
	 }

# pf new-packet.pf
mainmenu

